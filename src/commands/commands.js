/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const menu = require("../utils/keyboards");
const { tokensDB, peopleDB } = require("../../main");
const handleErr = require("../utils/handleError");
const tokensList = require("../utils/tokensList");
const { startMessage } = require("../utils/messages");
const reply = require("../utils/sendMessageText");

module.exports = {
  setCommands: function (bot) {
    bot.command("start", (ctx) => reply(ctx, startMessage, menu).catch((err) => handleErr(err, ctx)));

    bot.command("info", async (ctx) => {
      if (ctx.from.id != process.env.ADMIN_ID) return;

      try {
        const people = await peopleDB.find();
        await reply(
          ctx,
          Object.values(people).filter((v) => {
            if (Object.values(v).length > 1) return true;
          }).length + " users using the bot."
        );
      } catch (err) {
        handleErr(err, ctx);
      }
    });

    bot.command("updateList", async (ctx) => {
      try {
        await reply(ctx, "Ok. This will take just a second...");
        const list = await tokensList.getAllTokens();

        let addedCount = 0;
        const entries = Object.entries(list);
        for (const [
          id,
          { PDecimals: decimals, PSymbol: pSymbol, Symbol: symbol, Name: name, Verified: verified },
        ] of entries) {
          const isInPool = await tokensList.doesPairExists(id);
          if (isInPool && (pSymbol || symbol) && name && id) {
            let tokenInfo = await tokensDB.findOne({ _id: id });
            if (!tokenInfo) {
              await tokensDB.update(
                { _id: id },
                {
                  _id: id,
                  symbol: pSymbol || symbol,
                  name,
                  decimals: decimals || 0,
                  people: 0,
                  verified: Boolean(pSymbol) || verified,
                },
                { upsert: true }
              );
              addedCount++;
              let coinsWithUsers = await tokensDB.findOne({ _id: "users" });
              if (!(id in (coinsWithUsers || {}))) {
                await tokensDB.update({ _id: "users" }, { $set: { [id]: [] } }, { upsert: true });
              }
            }
          }
        }

        await reply(ctx, `Done. ${addedCount} ${addedCount === 1 ? "coin was" : "new coins were"} added.`);
      } catch (err) {
        handleErr(err, ctx);
      }
    });
  },
};
