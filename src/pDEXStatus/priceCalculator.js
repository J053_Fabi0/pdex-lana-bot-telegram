/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const tokensList = require("../utils/tokensList");
const { pricesDB, tokensDB } = require("../../main");
const { leading0s } = require("../utils/numbersStrings");

const a = {};

a.transactionFee = 100;

const showConsoleLogs = false;
async function getTokenPrices({
  tokenSymbol,
  tokenID,
  tokenDecimals,
  PRVToSell = null,
  PRVToBuy = null,
  tokensToSell = null,
  tokensToBuy = null,
  PRV,
  token,
  truncateToMaxDecimals = true,
  addFees = true,
  roundOne = true,
}) {
  if (tokenSymbol) {
    tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);
    const { intToken, intPRV } = await pricesDB.findOne({ _id: tokenID });
    PRV = intPRV;
    token = intToken;
    const { decimals } = await tokensDB.findOne({ _id: tokenID });
    tokenDecimals = decimals;
  }
  PRV = BigInt(PRV);
  token = BigInt(token);
  tokenDecimals = Number(tokenDecimals);

  const intConstant = PRV * token;

  const fixedDecimals = truncateToMaxDecimals ? 9 : tokenDecimals;

  let returnValue = BigInt(0);
  // How many tokens you need to buy X PRV.
  if (PRVToBuy !== null) {
    PRVToBuy = BigInt(PRVToBuy.moveDecimalDot(9));
    if (showConsoleLogs) {
      console.log("-----------------");
      console.log("Buy PRV");
      console.log("intConstant =", intConstant);
      console.log("PRV =", PRV);
      console.log("PRVToBuy =", PRVToBuy);
      console.log("token =", token);
    }
    returnValue = intConstant / (PRVToBuy < PRV ? PRV - PRVToBuy : 1n) - token + (roundOne ? 1n : 0n);
    returnValue = returnValue.moveDecimalDot(-tokenDecimals);
  }
  // How many PRV you need to buy X tokens.
  if (tokensToBuy !== null) {
    tokensToBuy = BigInt(tokensToBuy.moveDecimalDot(tokenDecimals));
    if (showConsoleLogs) {
      console.log("-----------------");
      console.log("Buy tokens");
      console.log("intConstant =", intConstant);
      console.log("token =", token);
      console.log("tokensToBuy =", tokensToBuy);
      console.log("PRV =", PRV);
    }
    returnValue = intConstant / (tokensToBuy < token ? token - tokensToBuy : 1n) - PRV;
    if (returnValue >= 0)
      returnValue = (
        returnValue +
        (roundOne ? 1n : 0n) +
        BigInt(addFees ? a.transactionFee * 4 : 0)
      ).moveDecimalDot(-9);
  }

  // How many PRV you need to sell to get X tokens.
  if (PRVToSell !== null) {
    PRVToSell = BigInt(PRVToSell.moveDecimalDot(9));
    if (showConsoleLogs) {
      console.log("-----------------");
      console.log("Sell PRV");
      console.log("token =", token);
      console.log("intConstant =", intConstant);
      console.log("PRV =", PRV);
      console.log("PRVToSell =", PRVToSell);
    }
    returnValue =
      token - intConstant / (PRV + PRVToSell - BigInt(addFees ? a.transactionFee * 4 : 0)) - (roundOne ? 1n : 0n);
    returnValue = (returnValue <= 0 ? "0" : returnValue).moveDecimalDot(-tokenDecimals);
  }
  // How many tokens you need to sell to get X PRV.
  if (tokensToSell !== null) {
    if (showConsoleLogs) {
      console.log("-----------------");
      console.log("Sell tokens");
      console.log("PRV =", PRV);
      console.log("intConstant =", intConstant);
      console.log("token =", token);
      console.log("tokensToSell =", tokensToSell);
    }
    returnValue = (
      PRV -
      intConstant / (token + BigInt(tokensToSell.moveDecimalDot(tokenDecimals))) -
      1n
    ).moveDecimalDot(-9);
  }

  if (showConsoleLogs) {
    console.log("returnValue", returnValue);
    console.log("returnValue fixed", leading0s(returnValue.toFixedS(fixedDecimals)));
  }

  if (returnValue < 0.000000001) return "0";
  return leading0s(returnValue.toFixedS(fixedDecimals));
}

/**
 * How many tokens you need to sell to get X PRV. Symbol is required if the token info is not given.
 */
a.priceOfSellXPRV = async function ({
  tokenID,
  tokenDecimals,
  PRVToSell,
  PRV,
  token,
  truncateToMaxDecimals = true,
  tokenSymbol,
  addFees = true,
  roundOne = true,
}) {
  const res = await getTokenPrices({
    ...{ tokenID, tokenDecimals, PRVToSell },
    ...{ PRV, token, truncateToMaxDecimals, tokenSymbol, addFees, roundOne },
  });
  return res;
};

/**
 * How many PRV you need to sell to get X tokens. Symbol is required if the token info is not given.
 */
a.priceOfSellXTokens = async function ({
  tokenID,
  tokenDecimals,
  tokensToSell,
  PRV,
  token,
  truncateToMaxDecimals = true,
  tokenSymbol,
}) {
  const res = await getTokenPrices({
    ...{ tokenID, tokenDecimals, tokensToSell },
    ...{ PRV, token, truncateToMaxDecimals, tokenSymbol },
  });
  return res;
};

/**
 * How many tokens you need to buy X PRV. Symbol is required if the token info is not given.
 */
a.priceToBuyXPRV = async function ({
  tokenID = "",
  tokenDecimals,
  PRVToBuy,
  PRV,
  token,
  truncateToMaxDecimals = true,
  tokenSymbol,
  roundOne = true,
}) {
  const res = await getTokenPrices({
    ...{ tokenID, tokenDecimals, PRVToBuy },
    ...{ PRV, token, truncateToMaxDecimals, tokenSymbol, roundOne },
  });
  return res;
};

/**
 * How many PRV you need to buy X tokens. Symbol is required if the token info is not given.
 */
a.priceToBuyXTokens = async function ({
  tokenID,
  tokenDecimals,
  tokensToBuy,
  PRV,
  token,
  truncateToMaxDecimals = true,
  tokenSymbol,
  addFees = true,
  roundOne = true,
}) {
  const res = await getTokenPrices({
    ...{ tokenID, tokenDecimals, tokensToBuy },
    ...{ PRV, token, truncateToMaxDecimals, tokenSymbol, addFees, roundOne },
  });
  return res;
};

a.getMinPrvToSell = () => (a.transactionFee * 4 + 1).moveDecimalDot(-9);

module.exports = a;
