/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const axios = require("axios");
const handleErr = require("../utils/handleError");
const tokensList = require("../utils/tokensList");
const { specialEmoji: SE } = require("../utils/messages");
const { pricesDB, tokensDB, peopleDB } = require("../../main");
const priceCalculator = require("../pDEXStatus/priceCalculator");
const reply = require("../utils/sendMessageText");
const { numberWithCommas, leading0s } = require("../utils/numbersStrings");

const SECOND = 1000;
const TIME_BETWEEN_CHECKS = 10 * SECOND;

let height, status;
const fixedDecimals = 4;
const PRV_ID = "0000000000000000000000000000000000000000000000000000000000000004";

async function getFullpDEXStatus(height) {
  const id = Math.floor(Math.random() * 10000);
  if (process.env.VERBOSE) console.log("A request to get the full pDEX state has started. ID: " + id);

  const options = {
    method: "POST",
    url: process.env.FULLNODE_DIRECTION,
    data: {
      id: 1,
      jsonrpc: "1.0",
      method: "getpdestate",
      params: [
        {
          BeaconHeight: height,
        },
      ],
    },
  };

  const res = await axios(options);
  if (process.env.VERBOSE) console.log("The request to get the full pDEX state has ended. ID: " + id);

  if (res.data.Error) throw res.data.Error;

  return res.data?.Result;
}

async function getBeaconHeight() {
  const id = Math.floor(Math.random() * 10000);
  if (process.env.VERBOSE) console.log("A request to get the beacon height has started. ID: " + id);

  const options = {
    method: "get",
    url: process.env.FULLNODE_DIRECTION,
    data: {
      id: 1,
      jsonrpc: "1.0",
      method: "getblockchaininfo",
      params: [],
    },
  };

  const res = await axios(options);
  if (process.env.VERBOSE) console.log("The request to get the beacon height has ended. ID: " + id);

  if (res.data.Error) throw res.data.Error;

  return res.data?.Result?.BestBlocks?.[-1]?.Height;
}

const a = {};

async function getTokenPrices({ tokenID, tokenDecimals }) {
  const poolInfo = status?.PDEPoolPairs?.[`pdepool-${height}-${PRV_ID}-${tokenID}`];

  if (!poolInfo) throw "No pool info for " + tokenID;

  const { Token1PoolValue: PRV, Token2PoolValue: token } = poolInfo;

  const divisorPRV = 1000000000;
  let divisorToken = 1;
  for (let i = 0; i < tokenDecimals; i++) divisorToken *= 10;
  const finalDivisor = divisorPRV / divisorToken;

  return {
    price: ((token / PRV) * finalDivisor).toFixedS(9),
    reversePrice: (PRV / token / finalDivisor).toFixedS(9),
    constant: ((PRV * token) / (divisorToken * divisorPRV)).toFixedS(9),
    PRV: (PRV / divisorPRV).toFixedS(9),
    token: (token / divisorToken).toFixedS(9),
    ...{ intPRV: PRV.toString(), intToken: token.toString() },
  };
}

// Call this function every time new data is received
async function processNewStatusInfo({ tokenSymbol, tokenID, pairInfo, tokenDecimals }) {
  const oldData = await pricesDB.findOne({ _id: tokenID });
  const oldPrice = oldData?.price.toFixedS(9) || "0";
  const oldReversePrice = oldData?.reversePrice.toFixedS(9) || "0";
  let { PRV: oldPRV, token: oldToken, intToken: oldIntToken, intPRV: oldIntPRV } = oldData || {
    PRV: "0",
    token: "0",
    intToken: "0",
    intPRV: "0",
  };

  if (oldIntPRV !== pairInfo.intPRV || oldIntToken !== pairInfo.intToken) {
    const pairInfoToSave = { ...pairInfo };
    delete pairInfoToSave.constant;
    await pricesDB.update(
      { _id: tokenID },
      {
        $set: {
          ...pairInfoToSave,
          ...{ oldPrice, oldReversePrice, oldPRV, oldToken, oldIntToken, oldIntPRV },
        },
      },
      { upsert: true }
    );

    const { [tokenID]: people } = await tokensDB.findOne({ _id: "users" });
    for (const userID of people) {
      peopleDB
        .findOne({ _id: userID.toString() })
        .then(async ({ [tokenID]: { reverse, decimals = 4, notifications = true, customAlerts = [] } }) => {
          try {
            if (!notifications) return;

            const setAsInactive = async (i) => {
              customAlerts[i].active = false;
              await peopleDB.update({ _id: userID }, { $set: { [tokenID + ".customAlerts"]: customAlerts } });
            };
            let afterTheMessage;

            for (let i = 0; i < customAlerts.length; i++) {
              // If there is an everyTrade alert, break the for and send the message
              if (customAlerts[i].type === "everyTrade") break;

              // If the alert is active, the type is specificPrice and the eval return true
              if (
                customAlerts[i].active &&
                customAlerts[i].type === "specificPrice" &&
                eval(
                  `(customAlerts[i].reverse ? pairInfo.reversePrice : pairInfo.price) ${customAlerts[i].operation} customAlerts[i].wantedPrice`
                )
              ) {
                if (customAlerts[i].onlyOnce) await setAsInactive(i);
                break;
              }

              // If the alert is a customTrade alert and is active
              if (customAlerts[i].active && customAlerts[i].type === "customTrade") {
                const tknDecmls = await tokensList.getTokenDecimalsFromSymbol(tokenSymbol);
                let { prv, token, action, onlyOnce } = customAlerts[i];

                if (action == "SellPRV") {
                  let tokensToGet = await priceCalculator.priceOfSellXPRV({
                    tokenID,
                    tokenDecimals: tknDecmls,
                    PRVToSell: prv,
                    PRV: pairInfo.intPRV,
                    token: pairInfo.intToken,
                  });

                  // If the expected tokens are met
                  if (BigInt(tokensToGet.moveDecimalDot(tknDecmls)) > BigInt(token.moveDecimalDot(tknDecmls))) {
                    if (onlyOnce) await setAsInactive(i);
                    let priceToBuyTokens = await priceCalculator.priceToBuyXTokens({
                      tokenID,
                      tokenDecimals: tknDecmls,
                      tokensToBuy: token,
                      PRV: pairInfo.intPRV,
                      token: pairInfo.intToken,
                    });
                    priceToBuyTokens = numberWithCommas(priceToBuyTokens);

                    afterTheMessage = () =>
                      reply(
                        userID,
                        `\u{1F6A8} <b>The alert of selling <code>${numberWithCommas(prv)}</code> PRV to get ` +
                          `<code>${numberWithCommas(token)}</code> ${tokenSymbol} (or more) ` +
                          `<u>has been met</u>\u203C</b>\n\n` +
                          (BigInt(tokensToGet.moveDecimalDot(tknDecmls) > BigInt(token.moveDecimalDot(tknDecmls)))
                            ? `In fact, if you sell <code>${numberWithCommas(prv)}</code> PRV, you'll get ` +
                              `<code>${numberWithCommas(tokensToGet)}</code> ${tokenSymbol}.\n` +
                              `And you can buy <code>${numberWithCommas(token)}</code> ${tokenSymbol} ` +
                              `selling just <code>${priceToBuyTokens}</code> PRV.`
                            : "") +
                          (onlyOnce
                            ? "\n\nThis alert is now turned off. If you want, go to <i>Custom alerts</i> to activate it again."
                            : ""),
                        {
                          reply_markup: {
                            inline_keyboard: [
                              [
                                {
                                  text: `\u{1F525} See details \u{1F525}`,
                                  callback_data: `ce_${tokenSymbol}_false_${reverse}_${prv}_Sell`,
                                },
                              ],
                            ],
                          },
                          parse_mode: "HTML",
                        }
                      ).catch((err) => handleReplyError(err));
                    break;
                  }
                }
                if (action == "BuyPRV") {
                  const prvFromSellingTokens = await priceCalculator.priceOfSellXTokens({
                    tokenID,
                    tokenDecimals: tknDecmls,
                    tokensToSell: token,
                    PRV: pairInfo.intPRV,
                    token: pairInfo.intToken,
                  });

                  // If the expected prv are met
                  if (BigInt(prvFromSellingTokens.moveDecimalDot(9)) > BigInt(prv.moveDecimalDot(9))) {
                    if (onlyOnce) await setAsInactive(i);
                    let tokensToBuyPRV = await priceCalculator.priceToBuyXPRV({
                      tokenID,
                      tokenDecimals: tknDecmls,
                      PRVToBuy: prv,
                      PRV: pairInfo.intPRV,
                      token: pairInfo.intToken,
                    });
                    tokensToBuyPRV = numberWithCommas(tokensToBuyPRV);
                    token = numberWithCommas(token);

                    afterTheMessage = () =>
                      reply(
                        userID,
                        `\u{1F6A8} <b>The alert of buying <code>${numberWithCommas(
                          prv
                        )}</code> PRV selling <code>${token}</code> ` +
                          `${tokenSymbol} (or less) <u>has been met</u>\u203C</b>\n\n` +
                          (BigInt(prvFromSellingTokens.moveDecimalDot(9) > BigInt(prv.moveDecimalDot(9)))
                            ? `In fact, if you sell <code>${token}</code> ${tokenSymbol} you'll get ` +
                              `<code>${numberWithCommas(prvFromSellingTokens)}</code> PRV.\n` +
                              `And you can buy <code>${numberWithCommas(prv)}</code> PRV ` +
                              `selling just <code>${tokensToBuyPRV}</code> ${tokenSymbol}.`
                            : "") +
                          (onlyOnce
                            ? "\n\nThis alert is now turned off. If you want, go to <i>Custom alerts</i> to activate it again."
                            : ""),
                        {
                          reply_markup: {
                            inline_keyboard: [
                              [
                                {
                                  text: `\u{1F525} See details \u{1F525}`,
                                  callback_data: `ce_${tokenSymbol}_false_${reverse}_${prv}_Buy`,
                                },
                              ],
                            ],
                          },
                          parse_mode: "HTML",
                        }
                      ).catch((err) => handleReplyError(err));
                    break;
                  }
                }
              }

              // If the for reaches its end and none of the above if statements broke the for, then do not send a message to this user
              if (i + 1 === customAlerts.length) return;
            }

            const message = await a.getPairInfo({
              ...pairInfo,
              ...{ oldPrice, oldReversePrice, oldPRV, oldToken, oldIntToken, oldIntPRV },
              ...{ tokenDecimals, tokenSymbol, reverse, decimals },
            });

            reply(userID, message, {
              reply_markup: {
                inline_keyboard: [
                  [
                    {
                      text: `\u2699 Options ${SE}`,
                      callback_data: `goPair_${tokenSymbol}_false`,
                    },
                  ],
                ],
              },
              parse_mode: "MarkdownV2",
              disable_web_page_preview: true,
            })
              .catch((err) => handleReplyError(err))
              .finally(() => afterTheMessage?.());

            const handleReplyError = async (err) => {
              try {
                if (
                  err?.response?.description === "Forbidden: bot was blocked by the user" ||
                  err?.response?.description === "Bad Request: chat not found"
                ) {
                  await tokensDB.update({ _id: tokenID }, { $inc: { people: -1 } });
                  await tokensDB.update({ _id: "users" }, { $pull: { [tokenID]: userID.toString() } });

                  await peopleDB.update({ _id: userID.toString() }, { $unset: { [tokenID]: true } });
                  handleErr(`${userID} blocked us, but I unsubscribed him from ${tokenSymbol}.`, null, true);
                } else handleErr(err, null, true);
              } catch (err) {
                handleErr(err, null, true);
              }
            };
          } catch (err) {
            handleErr(err, null, true);
          }
        });
    }
  }
}

async function checkPDEX() {
  const coinsToListen = await tokensDB.find({ people: { $gte: 1 } });

  for (const { symbol: tokenSymbol, _id: tokenID, decimals: tokenDecimals } of coinsToListen) {
    const pairInfo = await getTokenPrices({ tokenID, tokenDecimals });

    await processNewStatusInfo({ tokenSymbol, tokenID, tokenDecimals, pairInfo });
  }
}

/**
 * If this function is used to load the info from DB and not feed directly with the data, tokenSymbol and userID are required.
 */
a.getPairInfo = async function ({
  oldPrice = null,
  oldReversePrice = null,
  oldToken = null,
  oldIntToken = null,
  oldPRV = null,
  oldIntPRV = null,
  constant = null,
  PRV = null,
  intPRV = null,
  token = null,
  intToken = null,
  price = null,
  reversePrice = null,
  tokenSymbol,
  tokenDecimals = null,
  tokenID = null,
  decimals = fixedDecimals,
  reverse = false,
  userID = null,
}) {
  // Get all the data from the database if userID is given
  if (userID && tokenSymbol) {
    tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);

    const tokenInfo = await tokensDB.findOne({ _id: tokenID });
    tokenDecimals = tokenInfo.decimals;

    const { [tokenID]: userOptions } = await peopleDB.findOne({ _id: userID.toString() });
    decimals = userOptions.decimals;
    reverse = userOptions.reverse;

    const data = await pricesDB.findOne({ _id: tokenID });
    if (!data) return "Loading prices\\. Please try again in a minute\\.";
    oldPrice = data.oldPrice;
    oldReversePrice = data.oldReversePrice;
    oldToken = data.oldToken;
    oldIntToken = data.oldIntToken;
    oldPRV = data.oldPRV;
    oldIntPRV = data.oldIntPRV;
    PRV = data.PRV;
    intPRV = data.intPRV;
    token = data.token;
    intToken = data.intToken;
    price = data.price;
    reversePrice = data.reversePrice;
  }

  constant = token * PRV;
  oldPrice = Number(oldPrice);
  oldReversePrice = Number(oldReversePrice);
  oldToken = Number(oldToken);
  oldIntToken = Number(oldIntToken);
  oldPRV = Number(oldPRV);
  oldIntPRV = Number(oldIntPRV);
  PRV = Number(PRV);
  intPRV = Number(intPRV);
  token = Number(token);
  intToken = Number(intToken);

  price = reverse ? Number(reversePrice) : Number(price);
  oldPrice = reverse ? oldReversePrice : oldPrice;

  let priceToShow = price.toFixedS(decimals);
  for (let i = decimals + 1; Number(priceToShow) === 0 && i <= 9; i++) priceToShow = price.toFixed(i);
  priceToShow = numberWithCommas(priceToShow.toFixedS(reverse ? 9 : tokenDecimals));

  let change, changeToShow, percentage, percentageToShow, oldPriceToShow;
  if (oldIntToken != "0") {
    change = Number(Math.abs(oldPrice - price).toFixedS(9));
    percentage = Number(((change * 100) / oldPrice).toFixedS(9));

    percentageToShow = percentage.toFixed(2);
    for (let i = 3; Number(percentageToShow) === 0 && i <= 9; i++) percentageToShow = percentage.toFixed(i);
    percentageToShow = numberWithCommas(leading0s(percentageToShow));

    changeToShow = change.toFixed(decimals);
    for (let i = decimals + 1; Number(changeToShow) === 0 && i <= 9; i++) changeToShow = change.toFixed(i);
    changeToShow = numberWithCommas(leading0s(changeToShow));

    oldPriceToShow = oldPrice.toFixed(decimals);
    for (let i = decimals + 1; Number(oldPriceToShow) === 0 && i <= 9; i++) oldPriceToShow = oldPrice.toFixed(i);
    oldPriceToShow = numberWithCommas(oldPriceToShow.toFixedS(reverse ? 9 : tokenDecimals));
  }

  // Simple price representation and if it went up or down
  let message =
    `*${reverse ? "\\#" + tokenSymbol : "PRV"}*: \`${priceToShow}\` ` +
    `${reverse ? "PRV" : "\\#" + tokenSymbol}\\.\n` +
    (percentageToShow !== "Infinity" && change
      ? `${oldPrice < price ? "\u2934 `+" : "\u2935 `-"}${changeToShow}\` \\(\`${percentageToShow}\`%\\)\\.\n` +
        `*Last price*: \`${oldPriceToShow}\` ${reverse ? "PRV" : tokenSymbol}\\.\n\n`
      : "\n");

  let tokensDifference = Math.abs((oldToken || 0) - token).toFixed(decimals);
  for (let i = decimals + 1; Number(tokensDifference) === 0 && i <= 9; i++)
    tokensDifference = Math.abs((oldToken || 0) - token).toFixed(i);
  tokensDifference = numberWithCommas(leading0s(tokensDifference));

  let PRV_Difference = Math.abs((oldPRV || 0) - PRV).toFixed(decimals);
  for (let i = decimals + 1; Number(PRV_Difference) === 0 && i <= 9; i++)
    PRV_Difference = Math.abs((oldPRV || 0) - PRV).toFixed(i);
  PRV_Difference = numberWithCommas(leading0s(PRV_Difference));

  // Transaction made info.
  if (oldIntToken !== 0) {
    if (!(((oldIntToken || 0) >= intToken) ^ ((oldIntPRV || 0) >= intPRV))) {
      // If it was a liquidity change
      const addedLiquidity = (oldIntToken || 0) < intToken && (oldIntPRV || 0) < intPRV;
      message +=
        `[Someone ${addedLiquidity ? "provided" : "removed"} ` +
        `liquidity](https://telegra.ph/Did-Lana-Bot-missed-a-transaction-12-20):\n` +
        `*PRV*: \`${addedLiquidity ? "+" : "-"}${PRV_Difference}\` and` +
        ((oldIntToken || 0) - intToken === 0
          ? `\\.\\.\\. \`0\` *${tokenSymbol}*\\.\\.\\. Someone have just lost some ${tokenSymbol} then, haha\\.\n\n`
          : ` *${tokenSymbol}*: \`${addedLiquidity ? "+" : "-"}` + `${tokensDifference}\`\\.\n\n`);
    } else {
      // Or if it was a sale or a buy
      const tokenBuy = (oldIntToken || 0) > intToken;
      message +=
        `[Someone ${reverse ? (tokenBuy ? "bought" : "sold") : tokenBuy ? "sold" : "bought"}]` +
        `(https://telegra.ph/Did-Lana-Bot-missed-a-transaction-12-20) ` +
        `\`${reverse ? tokensDifference : PRV_Difference}\` ${reverse ? tokenSymbol : "PRV"} ` +
        `${reverse ? (tokenBuy ? "with" : "for") : tokenBuy ? "for" : "with"} ` +
        `\`${reverse ? PRV_Difference : tokensDifference}\` ${reverse ? "PRV" : tokenSymbol}\\.\n\n`;
    }
  }

  let PRV_ToShow = PRV.toFixed(decimals);
  for (let i = decimals + 1; Number(PRV_ToShow) === 0 && i <= 9; i++) PRV_ToShow = PRV.toFixed(i);
  PRV_ToShow = numberWithCommas(leading0s(PRV_ToShow));

  let tokenToShow = token.toFixed(decimals);
  for (let i = decimals + 1; Number(tokenToShow) === 0 && i <= 9; i++) tokenToShow = token.toFixed(i);
  tokenToShow = numberWithCommas(leading0s(tokenToShow));

  // Liquidity info
  message +=
    `__*Liquidity pool*__\n*PRV* \`${PRV_ToShow}\`\\. *${tokenSymbol}*: \`${tokenToShow}\`\\.\n` +
    `*[Constant](https://youtu\\.be/AgCEe31b90U)*: \`${numberWithCommas(Number(constant.toFixed(3)))}\`\\.`;

  return message;
};

/**
 * Get the latest full pDEX status
 */
a.getFullpDEXStatus = () =>
  new Promise((resolve, reject) => {
    if (status) return resolve(status);

    a.getBeaconHeight()
      .then((height) =>
        getFullpDEXStatus(height)
          .then((status) => resolve(status))
          .catch((err) => reject(err))
      )
      .catch((err) => reject(err));
  });

/**
 * Get the latest full pDEX status
 */
a.getBeaconHeight = () =>
  new Promise((resolve, reject) => {
    if (height) return resolve(height);

    getBeaconHeight()
      .then((height) => resolve(height))
      .catch((err) => reject(err));
  });

/**
 * Call this function just once when the bot starts
 */
a.main = function (start = true, checkCount = 1) {
  if (!start) return;
  setTimeout(async () => {
    if (process.env.VERBOSE) console.log("-----------------------");
    if (process.env.VERBOSE) console.log("Check " + checkCount++);
    try {
      height = await getBeaconHeight();
      status = await getFullpDEXStatus(height);
      await checkPDEX();
    } catch (err) {
      handleErr(err, null, true);
    } finally {
      a.main(true, checkCount);
    }
  }, TIME_BETWEEN_CHECKS);
};

module.exports = a;
