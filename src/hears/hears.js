/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const handleErr = require("../utils/handleError");
const { specialEmoji: SE } = require("../utils/messages");
const mySubscriptions = require("../scenes/mySubscriptions");

module.exports = {
  setHears: function (bot) {
    bot.hears(/.*/, (ctx, next) => mySubscriptions.actionCalculator(ctx, next, false));
    bot.hears(/.*/, (ctx, next) => mySubscriptions.hearsCustomAlert(ctx, next, false));

    bot.hears([`\u{1F4E3} Subscribe to pair ${SE}`, "\u{1F4E3} Subscribe to pair"], (ctx) =>
      ctx.scene.enter("subscribeToPair").catch((err) => handleErr(err, ctx))
    );
    bot.hears([`\u{1F5DE} My subscriptions ${SE}`, "\u{1F5DE} My subscriptions"], (ctx) =>
      mySubscriptions.enter(ctx)
    );
  },
};
