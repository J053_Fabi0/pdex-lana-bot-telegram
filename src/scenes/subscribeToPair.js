/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const Extra = require("telegraf/extra");
const Markup = require("telegraf/markup");
const handleErr = require("../utils/handleError");
const { specialEmoji: SE } = require("../utils/messages");
const { tokensDB, peopleDB } = require("../../main");
const reply = require("../utils/sendMessageText");

const leaveButton = [Markup.callbackButton(`\u2716 Cancel ${SE}`, "leave")];

async function subscribeToPair({ tokenID, userID, tokenSymbol, ctx, editMessage }) {
  const users = await tokensDB.findOne({ _id: "users" });
  if (users?.[tokenID]?.includes(userID.toString())) {
    if (editMessage) ctx.answerCbQuery("That token is already in your list. No need to add it again.", true);
    else reply(ctx, `You were already subscribed to the *PRV-${tokenSymbol}* pair.`, { parse_mode: "Markdown" });
    return;
  }

  await tokensDB.update({ _id: tokenID }, { $inc: { people: 1 } });
  await tokensDB.update({ _id: "users" }, { $addToSet: { [tokenID]: userID.toString() } });
  await peopleDB.update(
    { _id: userID.toString() },
    {
      $set: {
        [tokenID]: {
          reverse: false,
          decimals: 4,
          notifications: true,
          customAlerts: [{ type: "everyTrade", id: -1 }],
        },
      },
    },
    { upsert: true }
  );

  if (editMessage)
    await ctx.editMessageText(`You are now subscribed to the *PRV-${tokenSymbol}* pair.`, {
      parse_mode: "markdown",
    });
  else await reply(ctx, `You are now subscribed to the *PRV-${tokenSymbol}* pair.`, { parse_mode: "Markdown" });

  setTimeout(() => ctx.scene.leave(), 1000);
}

const a = {};

a.enter = (ctx) => {
  reply(
    ctx,
    "Send me the symbol or the name of the coin you want to be subscribed to.",
    Extra.markup(Markup.inlineKeyboard(leaveButton))
  ).catch((err) => handleErr(err, ctx));
};

a.actionSubscribeTo = async function (ctx) {
  try {
    const [, tokenSymbol] = ctx.update.callback_query.data.split("_");
    const res = await tokensDB.findOne({ symbol: tokenSymbol });
    const tokenID = res._id;
    await subscribeToPair({ tokenID, userID: ctx.from.id, tokenSymbol, ctx, editMessage: true });
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.hearsSymbol = async function (ctx) {
  try {
    const query = ctx.update.message.text;
    const symbolList = await tokensDB.find({ symbol: { $regex: new RegExp(query, "gi") } });
    const nameList = await tokensDB.find({ name: { $regex: new RegExp(query, "gi") } });
    let buttons = leaveButton;

    let list = [...symbolList, ...nameList];
    const namesSymbols = {};
    for (const { symbol, name, verified } of list) namesSymbols[name] = [symbol, verified];
    const numberOfResults = Object.keys(namesSymbols).length;

    let message = "";
    if (numberOfResults === 0)
      message +=
        `I didn't found any coin called *${query}* that has liquidity in the pDEX against PRV.\n` +
        `Maybe the database is outdated, so you could run the /updateList command and try again.`;
    if (numberOfResults >= 21)
      message += `I found a lot of results for *${query}*. Perhaps you can be more specific.`;

    if (numberOfResults >= 2 && numberOfResults <= 20) {
      message += `\n\nWhich of these ones is the *${query}* you are searching for?\n\u2705 = Verified`;
      buttons = [];
      for (const [name, [symbol, verified]] of Object.entries(namesSymbols)) {
        if (verified)
          buttons.push([Markup.callbackButton(`\n${name} (${symbol}) \u2705`, `subscribeTo_${symbol}`)]);
        else buttons.unshift([Markup.callbackButton(`\n${name} (${symbol})`, `subscribeTo_${symbol}`)]);
      }
      buttons.push(leaveButton);
    }

    if (numberOfResults === 1) {
      const { _id: id, symbol } = list[0];
      const { [id]: listPeople } = await tokensDB.findOne({ _id: "users" });
      if (listPeople.includes(ctx.from.id.toString())) message += "You are already subscribed to that pair.";
      else {
        await subscribeToPair({ tokenID: id, userID: ctx.from.id, tokenSymbol: symbol, ctx });
        return;
      }
    }

    await reply(ctx, message, Extra.markup(Markup.inlineKeyboard(buttons)).markdown());
  } catch (err) {
    handleErr(err, ctx);
  }
};

exports.subscribeToPair = a;
