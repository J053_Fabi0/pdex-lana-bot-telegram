/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram). If not, see <https://www.gnu.org/licenses/>.
*/

const Extra = require("telegraf/extra");
const Markup = require("telegraf/markup");
const { peopleDB, pricesDB } = require("../../main");
const handleErr = require("../utils/handleError");
const tokensList = require("../utils/tokensList");
const { specialEmoji: SE } = require("../utils/messages");
const reply = require("../utils/sendMessageText");
const { numberWithCommas } = require("../utils/numbersStrings");

const a = {};

const settingNewAlert = {}; // It is the name of the alert that is being set
const tokenSymbol = {};
const backHome = {};
const reverse = {};

const valueBeingEntered = {}; // It can be "prv" or "token"
const tradeAction = {};
const prvTrade = {};

const alertsLimit = 8; // The limit of alerts a user can have

a.actionMyCustomAlerts = async (ctx) => {
  const [, tokenSymbol, backHome, reverse] = ctx.update.callback_query.data.split("_");
  const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);
  let {
    [tokenID]: { customAlerts = [] },
  } = await peopleDB.findOne({ _id: ctx.from.id.toString() });
  settingNewAlert[ctx.from.id] = null;

  if (customAlerts.length === 0) {
    customAlerts = [{ type: "everyTrade", active: true, id: "-1" }];
    await peopleDB.update(
      { _id: ctx.from.id.toString() },
      { $addToSet: { [tokenID + ".customAlerts"]: customAlerts[0] } }
    );
  }

  const buttons = [];
  for (const alert of customAlerts) {
    let text = "";

    switch (alert.type) {
      case "specificPrice":
        text =
          (alert.active ? "\u{1F50A} " : "\u{1F507} ") +
          `${alert.reverse ? tokenSymbol : "PRV"} ${alert.operation === "<=" ? "≤" : "≥"} ` +
          (alert.wantedPrice + " ") +
          (!alert.reverse ? tokenSymbol : "PRV") +
          (alert.onlyOnce ? " \u{1F502}" : " \u{1F501}");
        break;

      case "customTrade":
        text =
          (alert.active ? "\u{1F50A} " : "\u{1F507} ") +
          `${alert.action === "SellPRV" ? "Sell" : "Buy"} ${alert.prv} ` +
          `${alert.action === "SellPRV" ? "to get ≥" : "selling ≤"} ${alert.token} ${tokenSymbol}` +
          (alert.onlyOnce ? " \u{1F502}" : " \u{1F501}");
        break;

      case "everyTrade":
        text = "\u{1F4E3} Every trade";
        break;

      default:
        text = "?";
    }

    buttons.push([Markup.callbackButton(text, `selectAlert_${tokenSymbol}_${backHome}_${reverse}_${alert.id}`)]);
  }

  buttons.push([
    Markup.callbackButton(`\u2B05 Go back ${SE}`, `backPairInfo_${tokenSymbol}_${backHome}`),
    Markup.callbackButton(`\u2795 New alert ${SE}`, `newCustomAlert_${tokenSymbol}_${backHome}_${reverse}`),
  ]);

  await ctx.editMessageText(
    buttons.length === 2 ? `This is your only alert ${SE}` : `These are your ${buttons.length - 1} alerts ${SE}`,
    Extra.markup(Markup.inlineKeyboard(buttons)).markdown()
  );
  ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
};

a.actionSelectAlert = async (ctx) => {
  try {
    settingNewAlert[ctx.from.id] = null;
    const [, tokenSymbol, backHome, reverse, alertID] = ctx.update.callback_query.data.split("_");
    const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);
    const {
      [tokenID]: { customAlerts = [] },
    } = await peopleDB.findOne({ _id: ctx.from.id.toString() });

    const buttons = [];
    let text = "Unknown type of alert.";

    const alert = customAlerts.find((a) => a.id == alertID);
    const buttonsInfo = `${tokenSymbol}_${backHome}_${reverse}_${alertID}`;

    if (alert.type === "specificPrice") {
      buttons.push([
        Markup.callbackButton(alert.operation === "<=" ? "≥" : "≤", `greaterOrLessThan_${buttonsInfo}`),
        Markup.callbackButton(alert.active ? "\u{1F507}" : "\u{1F50A}", `alertActive_${buttonsInfo}`),
        Markup.callbackButton(alert.onlyOnce ? "\u{1F501}" : "\u{1F502}", `onlyOnce_${buttonsInfo}`),
      ]);
      text =
        `<b>\u{1F52E} Specific price ${alert.onlyOnce ? "\u{1F502}" : "\u{1F501}"}</b>\n\n` +
        `${alert.onlyOnce ? "Only the first" : "Every"} trade in which the price of selling ` +
        `<code>1</code> ${alert.reverse ? tokenSymbol : "PRV"} is ` +
        `<b>${alert.operation === "<=" ? "lower" : "greater"}</b> or equal ` +
        `to <code>${alert.wantedPrice.toFixedS(9)}</code> ${alert.reverse ? "PRV" : tokenSymbol}` +
        (alert.onlyOnce ? ", silencing itself after that." : ".") +
        (alert.active ? "" : "\n\n\u{1F507} <b>Muted</b> \u{1F507}");
    } else if (alert.type === "customTrade") {
      let { action, prv, token, onlyOnce, active } = alert;
      prv = numberWithCommas(prv);
      token = numberWithCommas(token);

      buttons.push([
        Markup.callbackButton(alert.active ? "\u{1F507}" : "\u{1F50A}", `alertActive_${buttonsInfo}`),
        Markup.callbackButton(alert.onlyOnce ? "\u{1F501}" : "\u{1F502}", `onlyOnce_${buttonsInfo}`),
      ]);
      text =
        `<b>\u{1F4B1} Custom trade ${onlyOnce ? "\u{1F502}" : "\u{1F501}"}</b>\n\n` +
        `<b>Alert ${onlyOnce ? "only once when" : "every time that"}:</b>\n` +
        `· If you ${action == "SellPRV" ? "sell" : "buy"} <code>${prv}</code> PRV, ` +
        `${action == "SellPRV" ? "you'll get" : "you only need to pay"} <code>${token}</code>` +
        ` (or ${action == "SellPRV" ? "more" : "less"}) ${tokenSymbol}.\n` +
        "<u>Or</u> <i>(from another point of view)</i>:\n" +
        `· If you ${action == "SellPRV" ? "buy" : "sell"} <code>${token}</code> ${tokenSymbol}, ` +
        `${action == "SellPRV" ? "you only need to pay" : "you'll get"} <code>${prv}</code> ` +
        ` (or ${action == "SellPRV" ? "less" : "more"}) PRV.` +
        (active ? "" : "\n\n\u{1F507} <b>Muted</b> \u{1F507}");
    } else if (alert.type === "everyTrade") {
      text =
        `\u{1F4E3} <b>Every trade</b>\n\n` +
        (customAlerts.length === 1
          ? "To delete this alert, create a new custom alert."
          : "This alert is useful when you want to temporarily be notified about every trade, even if you have some custom alerts.");
    }

    const toPush = [
      Markup.callbackButton(`\u2B05 Go back`, `myCustomAlerts_${tokenSymbol}_${backHome}_${reverse}`),
    ];
    if (alert.type !== "everyTrade" || customAlerts.length >= 2) {
      toPush.push(Markup.callbackButton(`\u{1F5D1} Delete`, `deleteAlert_${buttonsInfo}`));
    }
    buttons.push(toPush);

    await ctx.editMessageText(text, Extra.markup(Markup.inlineKeyboard(buttons)).HTML());
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionAlertActive = async (ctx) => {
  try {
    await changeSomethigAlert({
      userID: ctx.from.id.toString(),
      CBQueryData: ctx.update.callback_query.data,
      change: "active",
    });

    a.actionSelectAlert(ctx);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionGreaterOrLessThan = async (ctx) => {
  try {
    await changeSomethigAlert({
      userID: ctx.from.id.toString(),
      CBQueryData: ctx.update.callback_query.data,
      change: "operation",
    });

    a.actionSelectAlert(ctx);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionOnlyOnce = async (ctx) => {
  try {
    await changeSomethigAlert({
      userID: ctx.from.id.toString(),
      CBQueryData: ctx.update.callback_query.data,
      change: "onlyOnce",
    });

    a.actionSelectAlert(ctx);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

async function changeSomethigAlert({ userID: id, CBQueryData, change }) {
  const [, tokenSymbol, , , alertID] = CBQueryData.split("_");
  const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);

  const {
    [tokenID]: { customAlerts = [] },
  } = await peopleDB.findOne({ _id: id });

  const indexOfAlert = customAlerts.findIndex((a) => a.id === alertID);
  switch (change) {
    case "active":
      customAlerts[indexOfAlert].active = !customAlerts[indexOfAlert].active;
      break;
    case "onlyOnce":
      customAlerts[indexOfAlert].onlyOnce = !customAlerts[indexOfAlert].onlyOnce;
      break;
    case "operation":
      customAlerts[indexOfAlert].operation = customAlerts[indexOfAlert].operation === "<=" ? ">=" : "<=";
  }

  await peopleDB.update({ _id: id }, { $set: { [tokenID + ".customAlerts"]: customAlerts } });
}

a.actionDeleteAlert = async (ctx) => {
  try {
    const [, tokenSymbol, , , alertID] = ctx.update.callback_query.data.split("_");
    const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);
    const id = ctx.from.id.toString();

    const {
      [tokenID]: { customAlerts = [] },
    } = await peopleDB.findOne({ _id: id });

    customAlerts.splice(
      customAlerts.findIndex((a) => a.id === alertID),
      1
    );

    await peopleDB.update({ _id: id }, { $set: { [tokenID + ".customAlerts"]: customAlerts } });

    a.actionMyCustomAlerts(ctx);
    ctx.answerCbQuery("The alert has been deleted").catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionNewCustomAlert = async (ctx) => {
  try {
    settingNewAlert[ctx.from.id] = null;
    const [, tokenSymbol, backHome, reverse] = ctx.update.callback_query.data.split("_");
    const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);
    const id = ctx.from.id.toString();

    const {
      [tokenID]: { customAlerts = [] },
    } = await peopleDB.findOne({ _id: id });

    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

    if (customAlerts.length >= alertsLimit) {
      await ctx.editMessageText(
        "Sorry, but you have reached the maximum number of alerts you can have. You can delete old alerts to add a new one.",
        Extra.markup(
          Markup.inlineKeyboard([
            Markup.callbackButton(`\u2B05 Go back`, `myCustomAlerts_${tokenSymbol}_${backHome}_${reverse}`),
          ])
        )
      );
      return;
    }

    ctx
      .editMessageText(
        "Select the type of alert that you want to set",
        Extra.markup(
          Markup.inlineKeyboard([
            [
              Markup.callbackButton(
                "\u{1F52E} Specific price",
                `specificPriceAlert_${tokenSymbol}_${backHome}_${reverse}`
              ),
            ],
            [
              Markup.callbackButton(
                "\u{1F4B1} Custom trade",
                `customTradeAlert_${tokenSymbol}_${backHome}_${reverse}_SellPRV_prv`
              ),
            ],
            [
              Markup.callbackButton(
                "\u{1F4E3} Every trade",
                `everyTradeAlert_${tokenSymbol}_${backHome}_${reverse}`
              ),
            ],
            [Markup.callbackButton(`\u2B05 Go back`, `myCustomAlerts_${tokenSymbol}_${backHome}_${reverse}`)],
          ])
        ).markdown()
      )
      .catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionEveryTradeAlert = async (ctx) => {
  const [, tokenSymbol, backHome, reverse] = ctx.update.callback_query.data.split("_");
  const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);
  await addAlert({ type: "everyTrade", userID: ctx.from.id, tokenID });
  ctx
    .editMessageText(
      "Alright. A new alert has been set.",
      Extra.markup(
        Markup.inlineKeyboard([
          Markup.callbackButton(
            `\u{1F6E0} See my alerts ${SE}`,
            `myCustomAlerts_${tokenSymbol}_${backHome}_${reverse}`
          ),
          Markup.callbackButton("\u2699 Options", `selectAlert_${tokenSymbol}_${backHome}_${reverse}_-1`),
        ])
      ).markdown()
    )
    .catch((err) => handleErr(err, ctx));
  ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
};

a.actionCustomTradeAlert = async (ctx) => {
  const [, ts, bh, rev, tradeAct, valueBE] = ctx.update.callback_query.data.split("_");
  const { id } = ctx.from;
  tokenSymbol[id] = ts;
  backHome[id] = bh;
  settingNewAlert[id] = "customTrade";
  tradeAction[id] = tradeAct;
  valueBeingEntered[id] = valueBE;

  try {
    await ctx.editMessageText(
      "<b>\u{1F4B1} Custom trade</b>\n\n" +
        `Send <b>how many ${valueBE === "prv" ? "PRV" : ts} ` +
        `you want to <u>${tradeAct === "SellPRV" ? "sell" : "buy"}</u></b>.\n` +
        (tradeAct === "SellPRV"
          ? "<i>Hence, you are buying tokens.</i>"
          : "<i>Hence, you are selling tokens.</i>"),
      Extra.markup(
        Markup.inlineKeyboard([
          [
            Markup.callbackButton(
              `${tradeAct == "SellPRV" ? "Buy" : "Sell"} ${SE}`,
              `customTradeAlert_${ts}_${bh}_${rev}_${tradeAct == "SellPRV" ? "BuyPRV" : "SellPRV"}_${valueBE}`
            ),
          ],
          [Markup.callbackButton(`\u2B05 Go back`, `newCustomAlert_${ts}_${bh}_${rev}`)],
        ])
      ).HTML()
    );
    await ctx.answerCbQuery();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionSpecificPriceAlert = async (ctx) => {
  const [, ts, bh, rev] = ctx.update.callback_query.data.split("_");
  const id = ctx.from.id;
  tokenSymbol[id] = ts;
  backHome[id] = bh;
  reverse[id] = rev === "true";
  settingNewAlert[id] = "specificPrice";
  const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol[id]);
  const { [reverse[id] ? "reversePrice" : "price"]: price } = await pricesDB.findOne({
    _id: tokenID,
  });

  ctx
    .editMessageText(
      "<b>\u{1F52E} Specific price</b>\n\n" +
        `Send <b>how many ${rev === "true" ? "PRV" : ts} you expect that will be needed ` +
        `to buy 1 ${rev === "false" ? "PRV" : ts}.</b>\n\n` +
        `Right now, 1 ${rev === "false" ? "PRV" : ts} can be bought with ` +
        `~<code>${price}</code> ${rev === "true" ? "PRV" : ts}`,
      Extra.markup(
        Markup.inlineKeyboard([
          [
            Markup.callbackButton(
              `\u{1F501} Reverse ${SE}`,
              `specificPriceAlert_${ts}_${bh}_${rev === "true" ? "false" : "true"}`
            ),
          ],
          [Markup.callbackButton(`\u2B05 Go back`, `newCustomAlert_${ts}_${bh}_${rev}`)],
        ])
      ).HTML()
    )
    .catch((err) => handleErr(err, ctx));
  ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
};

a.hearsCustomAlert = async (ctx, next) => {
  try {
    // If not setting an alert, return.
    if (typeof settingNewAlert[ctx.from.id] !== "string") return next(ctx);

    let updateMessageText = ctx.update.message.text.toFixedS(9);
    const { id } = ctx.from;
    let message = "?";
    let buttons = [];
    let addedAlertID;

    // If is not a posive number, is 0 or is greater than the limit
    if (
      /^(\d+\.?\d*|\d*\.?\d+)$/.test(updateMessageText) === false ||
      BigInt((updateMessageText = updateMessageText.toFixedS(9)).moveDecimalDot(9)) <= 0 ||
      BigInt(updateMessageText.moveDecimalDot(9)) >= BigInt("9007199254740991000000000".moveDecimalDot(9))
    ) {
      message =
        "Only positive numbers greater than `0.000000001` and less than `9007199254740991000000000`, please.";
      buttons = [
        Markup.callbackButton(
          `\u2B05 Go back ${SE}`,
          `newCustomAlert_${tokenSymbol[id]}_${backHome[id]}_${reverse[id]}`
        ),
      ];
    } else if (settingNewAlert[id] === "specificPrice") {
      // If the alert is specificPrice
      settingNewAlert[ctx.from.id] = null;
      updateMessageText = Number(updateMessageText);
      message = "Alright. A new alert has been set.";
      const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol[id]);
      const { [reverse[id] ? "reversePrice" : "price"]: price } = await pricesDB.findOne({
        _id: tokenID,
      });

      addedAlertID = await addAlert({
        type: "specificPrice",
        operation: updateMessageText >= price ? ">=" : "<=",
        wantedPrice: updateMessageText,
        userID: id,
        tokenID,
      });
      if (!addedAlertID) {
        message = "You reached the limit of alerts you can have.";
        buttons = [
          Markup.callbackButton(
            `\u{1F6E0} See my alerts ${SE}`,
            `myCustomAlerts_${tokenSymbol[id]}_${backHome[id]}_${reverse[id]}`
          ),
        ];
      }
    } else if (settingNewAlert[id] === "customTrade") {
      if (valueBeingEntered[id] === "prv") {
        prvTrade[id] = updateMessageText;
        valueBeingEntered[id] = "token";

        message =
          tradeAction[id] == "SellPRV"
            ? `Now, how many ${tokenSymbol[id]} you want to get from selling \`${updateMessageText}\` PRV?`
            : `Now, how many ${tokenSymbol[id]} you want to sell to get ` +
              `${Number(updateMessageText) === 1 ? "that" : "those"} \`${updateMessageText}\` PRV?`;

        buttons = [
          Markup.callbackButton(
            `\u2716 Cancel ${SE}`,
            `newCustomAlert_${tokenSymbol[id]}_${backHome[id]}_${reverse[id]}`
          ),
        ];
      } else {
        valueBeingEntered[id] = null;

        message = "Alright. A new alert has been set.";
        const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol[id]);
        const tokenDecimals = await tokensList.getTokenDecimalsFromSymbol(tokenSymbol[id]);
        updateMessageText = updateMessageText.toFixedS(tokenDecimals);

        addedAlertID = await addAlert({
          type: "customTrade",
          action: tradeAction[id],
          userID: id,
          prv: prvTrade[id],
          token: updateMessageText,
          tokenID,
        });
        if (!addedAlertID) {
          message = "You reached the limit of alerts you can have.";
          buttons = [
            Markup.callbackButton(
              `\u{1F6E0} See my alerts ${SE}`,
              `myCustomAlerts_${tokenSymbol[id]}_${backHome[id]}_${reverse[id]}`
            ),
          ];
        }
      }
    }

    if (buttons.length === 0) {
      buttons = [
        Markup.callbackButton(
          `\u{1F6E0} See my alerts ${SE}`,
          `myCustomAlerts_${tokenSymbol[id]}_${backHome[id]}_${reverse[id]}`
        ),
        Markup.callbackButton(
          "\u2699 Options",
          `selectAlert_${tokenSymbol[id]}_${backHome[id]}_${reverse[id]}_${addedAlertID}`
        ),
      ];
    }
    reply(ctx, message, Extra.markup(Markup.inlineKeyboard(buttons)).markdown()).catch((err) =>
      handleErr(err, ctx)
    );
  } catch (err) {
    handleErr(err, ctx);
  }
};

async function addAlert({ type, operation, wantedPrice, userID, tokenID, action, prv, token }) {
  const {
    [tokenID]: { customAlerts = [] },
  } = await peopleDB.findOne({ _id: userID.toString() });

  if (customAlerts.length >= alertsLimit && type !== "everyTrade") return false;

  if (customAlerts.find((a) => a.type === "everyTrade") && (customAlerts.length === 1 || type === "everyTrade")) {
    // If it has the everyTrade alert, remove it
    customAlerts.splice(
      customAlerts.findIndex((a) => a.type === "everyTrade"),
      1
    );
  }

  const id = Date.now().toString();

  if (type === "specificPrice" && (operation == "<=" || operation == ">="))
    customAlerts.push({
      reverse: Boolean(reverse[userID]),
      onlyOnce: false,
      active: true,
      wantedPrice,
      operation,
      type,
      id,
    });
  else if (type === "customTrade" && (action == "SellPRV" || action == "BuyPRV"))
    customAlerts.push({
      type: "customTrade",
      onlyOnce: true,
      active: true,
      action,
      token,
      prv,
      id,
    });
  else if (type === "everyTrade") customAlerts.push({ type, id: "-1" });
  else return false;

  await peopleDB.update({ _id: userID.toString() }, { $set: { [tokenID + ".customAlerts"]: customAlerts } });

  return id;
}

module.exports = a;
