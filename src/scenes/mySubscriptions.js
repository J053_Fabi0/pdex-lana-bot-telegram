/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram). If not, see <https://www.gnu.org/licenses/>.
*/

const Extra = require("telegraf/extra");
const Markup = require("telegraf/markup");
const calculator = require("./calculator");
const customAlerts = require("./customAlerts");
const handleErr = require("../utils/handleError");
const tokensList = require("../utils/tokensList");
const { specialEmoji: SE } = require("../utils/messages");
const { tokensDB, peopleDB } = require("../../main");
const pDEXStatus = require("../pDEXStatus/pDEXStatus");
const multiInherit = require("../utils/multiInherit");
const reply = require("../utils/sendMessageText");

const a = multiInherit(calculator, customAlerts);

let goPair = "";

async function home(ctx, editMessage = false) {
  const id = ctx.from.id;
  const userTokens = await peopleDB.findOne({ _id: id.toString() });
  const tokensInfoArr = await tokensDB.find({});
  const tokensInfo = {};
  for (const token of tokensInfoArr) tokensInfo[token._id] = token;
  a.usingCalculator[id] = false;
  a.reverse[id] = false;
  a.tradeAmount[id] = "1";
  a.action[id] = "Sell";

  const notSubscribed = "You are not subscribed to a pair yet.";
  if (!userTokens) return editMessage ? ctx.editMessageText(notSubscribed) : reply(ctx, notSubscribed);
  if (Object.keys(userTokens).length <= 1)
    return editMessage ? ctx.editMessageText(notSubscribed) : reply(ctx, notSubscribed);

  delete userTokens._id;
  const subscriptions = [];

  for (const token of Object.keys(userTokens)) {
    const tokenSymbol = tokensInfo[token].symbol;

    const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);
    const {
      [tokenID]: { notifications },
    } = await peopleDB.findOne({ _id: ctx.from.id.toString() });

    subscriptions.push([
      Markup.callbackButton(
        `${notifications ? "\u{1F50A}" : "\u{1F507}"} PRV-${tokenSymbol} ${SE}`,
        `select_${tokenSymbol}`
      ),
    ]);
  }
  subscriptions.push([Markup.callbackButton(`\u2716 Close`, "leave")]);

  const message = "These are your subscriptions \u{1F514}";
  if (editMessage)
    await ctx.editMessageText(message, Extra.markup(Markup.inlineKeyboard(subscriptions)).markdown());
  else await reply(ctx, message, Extra.markup(Markup.inlineKeyboard(subscriptions)).markdown());
}

async function showPairInfo({ ctx, tokenSymbol, editMessage = true, backHome = true, decimalsSettings = false }) {
  const id = ctx.from.id;
  const message = await pDEXStatus.getPairInfo({ tokenSymbol, userID: id });
  const isLoading = message === "Loading prices\\. Please try again in a minute\\.";
  const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);
  const {
    [tokenID]: { decimals, notifications, reverse },
  } = await peopleDB.findOne({ _id: id.toString() });
  a.usingCalculator[id] = false;

  let buttons = [];

  const backHomeCBData = backHome ? "backHome" : `hideOptions_${tokenSymbol}`;
  if (!decimalsSettings) {
    buttons = [
      [
        {
          text: `${SE} Min decimals: ${decimals}`,
          callback_data: isLoading ? backHomeCBData : `decimals_${tokenSymbol}_${backHome}`,
        },
        {
          text: `\u{1F501} Reverse ${SE}`,
          callback_data: isLoading ? backHomeCBData : `reverse_${tokenSymbol}_${backHome}`,
        },
      ],
      [
        {
          text: `\u{1F6E0} Custom alerts ${SE}`,
          callback_data: isLoading ? backHomeCBData : `myCustomAlerts_${tokenSymbol}_${backHome}_${reverse}`,
        },
        {
          text: `\u{1F4B1} Calculator ${SE}`,
          callback_data: isLoading
            ? backHomeCBData
            : `ce_${tokenSymbol}_${backHome}_${a.reverse[id] === null ? reverse : a.reverse[id]}_` +
              `${a.tradeAmount[id] || 2}_${a.action[id]}`,
        },
      ],
      [
        {
          text: notifications ? `\u{1F507} Silence ${SE}` : `\u{1F50A} Track again ${SE}`,
          callback_data: `notifications_${tokenSymbol}_${backHome}_${notifications}`,
        },
        {
          text: `\u{1f515} Unsubscribe ${SE}`,
          callback_data: `unsubscribe_${tokenSymbol}`,
        },
      ],
      [
        {
          text: `\u2B05 Go back`,
          callback_data: backHomeCBData,
        },
      ],
    ];
  } else {
    buttons = [
      [
        {
          text: `-1`,
          callback_data: `changeDecimals_${tokenSymbol}_${backHome}_-1_${decimals}`,
        },
        {
          text: `Ok: ${decimals}`,
          callback_data: `backPairInfo_${tokenSymbol}_${backHome}`,
        },
        {
          text: `+1`,
          callback_data: `changeDecimals_${tokenSymbol}_${backHome}_1_${decimals}`,
        },
      ],
    ];
  }

  const options = {
    reply_markup: {
      inline_keyboard: buttons,
    },
    parse_mode: "MarkdownV2",
    disable_web_page_preview: true,
  };

  if (editMessage) {
    await ctx.editMessageText(message, options);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } else reply(ctx, message, options);
}

a.enter = (ctx, editMessage = false) => {
  if (goPair) {
    const [tokenSymbol, backHome] = goPair.split("_");

    showPairInfo({
      ...{ ctx, tokenSymbol, editMessage: true },
      backHome: typeof backHome === "string" ? backHome === "true" : backHome,
    }).catch((err) => handleErr(err, ctx));

    goPair = "";
    return;
  }

  if (ctx.update?.callback_query) ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

  home(ctx, editMessage).catch((err) => handleErr(err, ctx));
};

a.actionSelectPair = ({ ctx, tokenSymbol = null, editMessage = true, backHome = true }) => {
  if (!tokenSymbol) tokenSymbol = ctx.update.callback_query.data.split("_")[1];
  showPairInfo({ ctx, tokenSymbol, editMessage, backHome, decimalsSettings: false }).catch((err) =>
    handleErr(err, ctx)
  );
  ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
};

a.actionNotifications = async (ctx) => {
  try {
    const [, tokenSymbol, backHome, notifications] = ctx.update.callback_query.data.split("_");

    const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);

    await peopleDB.update(
      { _id: ctx.from.id.toString() },
      { $set: { [tokenID + ".notifications"]: !(notifications === "true") } }
    );

    if (notifications === "true")
      await ctx.answerCbQuery(
        "Now you won't receive any notification from this pair, not even from your custom alerts.",
        true
      );

    showPairInfo({
      ...{ ctx, tokenSymbol, editMessage: true },
      backHome: backHome === "true",
    }).catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionUsubscribe = async (ctx) => {
  try {
    const [, tokenSymbol] = ctx.update.callback_query.data.split("_");

    const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);

    await tokensDB.update({ _id: tokenID }, { $inc: { people: -1 } });
    await tokensDB.update({ _id: "users" }, { $pull: { [tokenID]: ctx.from.id.toString() } });

    await peopleDB.update({ _id: ctx.from.id.toString() }, { $unset: { [tokenID]: true } });

    await ctx.answerCbQuery(`You are now unsubscribed from the PRV-${tokenSymbol} pair.`, true);
    await home(ctx, true);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionReverse = async (ctx) => {
  try {
    const [, tokenSymbol, backHome] = ctx.update.callback_query.data.split("_");

    const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);

    const {
      [tokenID]: { reverse },
    } = await peopleDB.findOne({ _id: ctx.from.id.toString() });
    await peopleDB.update({ _id: ctx.from.id.toString() }, { $set: { [tokenID + ".reverse"]: !reverse } });

    await showPairInfo({ ctx, tokenSymbol, editMessage: true, backHome: backHome === "true" });
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionHideOptions = async (ctx) => {
  try {
    const [, tokenSymbol] = ctx.update.callback_query.data.split("_");
    const message = await pDEXStatus.getPairInfo({ tokenSymbol, userID: ctx.from.id });
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

    const buttons = [
      [
        {
          text: `\u2699 Options `,
          callback_data: `goPair_${tokenSymbol}`,
        },
      ],
    ];

    await ctx.editMessageText(message, {
      reply_markup: {
        inline_keyboard: buttons,
      },
      parse_mode: "MarkdownV2",
      disable_web_page_preview: true,
    });

    await ctx.scene.leave();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionDecimals = (ctx) => {
  const [, tokenSymbol, backHome] = ctx.update.callback_query.data.split("_");

  showPairInfo({
    ...{ ctx, tokenSymbol, editMessage: true, decimalsSettings: true },
    backHome: backHome === "true",
  }).catch((err) => handleErr(err, ctx));
};

a.actionChangeDecimals = async (ctx) => {
  try {
    const [, tokenSymbol, backHome, change, decimals] = ctx.update.callback_query.data.split("_");

    const tokenID = await tokensList.getTokenIDFromSymbol(tokenSymbol);

    // Prevent the decimals to go bellow 0 or above the maximum decimals
    if (Number(decimals) + Number(change) <= -1) {
      await ctx.answerCbQuery("0 is the minimum.");
      return;
    }
    if (Number(decimals) + Number(change) > 9) {
      await ctx.answerCbQuery("9 is the maximum.");
      return;
    }

    await peopleDB.update(
      { _id: ctx.from.id.toString() },
      { $set: { [tokenID + ".decimals"]: Number(decimals) + Number(change) } }
    );

    showPairInfo({
      ...{ ctx, tokenSymbol, editMessage: true, decimalsSettings: true },
      backHome: backHome === "true",
    }).catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionBackPairInfo = (ctx) => {
  const [, tokenSymbol, backHome] = ctx.update.callback_query.data.split("_");

  showPairInfo({
    ...{ ctx, tokenSymbol, editMessage: true, decimalsSettings: false },
    backHome: backHome === "true",
  }).catch((err) => handleErr(err, ctx));
};

a.setGoPair = ({ tokenSymbol, backHome }) => (goPair = tokenSymbol + "_" + backHome);

module.exports = a;
