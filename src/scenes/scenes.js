/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const Scene = require("telegraf/scenes/base");

////////////////////////////////////////
//////// subscribeToPairScene //////////
////////////////////////////////////////
const { subscribeToPair } = require("./subscribeToPair");
const subscribeToPairScene = new Scene("subscribeToPair");

subscribeToPairScene.enter((ctx) => subscribeToPair.enter(ctx));
subscribeToPairScene.hears(/.*/, (ctx) => subscribeToPair.hearsSymbol(ctx));
subscribeToPairScene.action(/^subscribeTo_/, (ctx) => subscribeToPair.actionSubscribeTo(ctx));
subscribeToPairScene.on("message", (ctx) =>
  subscribeToPair.hearsInvalidSymbol(ctx, "Please send only text messages.")
);

exports.subscribeToPairScene = subscribeToPairScene;
