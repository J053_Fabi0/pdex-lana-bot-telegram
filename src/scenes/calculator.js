/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram). If not, see <https://www.gnu.org/licenses/>.
*/

require("../utils/bigIntFunctions");
const { pricesDB, bot } = require("../../main");
const handleErr = require("../utils/handleError");
const tokensList = require("../utils/tokensList");
const priceCalculator = require("../pDEXStatus/priceCalculator");
const { leading0s, numberWithCommas } = require("../utils/numbersStrings");
const reply = require("../utils/sendMessageText");

const a = {};
a.tokenSymbol = {};
a.backHome = {};
a.reverse = {};
a.tradeAmount = {};
a.action = {};
a.usingCalculator = {};

async function getMessageText(id) {
  const tokenDecimals = await tokensList.getTokenDecimalsFromSymbol(a.tokenSymbol[id]);
  const tokenID = await tokensList.getTokenIDFromSymbol(a.tokenSymbol[id]);
  let minToken = (1).moveDecimalDot(-tokenDecimals);
  a.tradeAmount[id] = a.tradeAmount[id].toFixedS(a.reverse[id] ? tokenDecimals : 9);

  const { intPRV, intToken } = await pricesDB.findOne({ _id: tokenID });

  let tradeResult = "0";
  let message = "";

  // Reverse means that is trading with the token, instead of doing calculations with PRV.
  // He wants to sell or buy tokens.
  if (a.reverse[id]) {
    // See if the tradeAmount is lower than the minimum possible amount for that token
    a.tradeAmount[id] =
      BigInt(a.tradeAmount[id].moveDecimalDot(tokenDecimals)) < 1
        ? (1).moveDecimalDot(-tokenDecimals)
        : a.tradeAmount[id];

    if (a.action[id] === "Sell") {
      // These are the PRV you get from selling those tokens
      tradeResult = await priceCalculator.priceOfSellXTokens({
        tokenSymbol: a.tokenSymbol[id],
        tokensToSell: a.tradeAmount[id],
      });

      // Lets see then how many tokens we get by selling those PRV
      const sellOfTrade = await priceCalculator.priceToBuyXPRV({
        tokenSymbol: a.tokenSymbol[id],
        PRVToBuy: tradeResult == "0" ? (1).moveDecimalDot(-9) : tradeResult,
        truncateToMaxDecimals: false,
        roundOne: true,
      });
      if (tradeResult == "0") (a.tradeAmount[id] = sellOfTrade), (tradeResult = (1).moveDecimalDot(-9));

      if (a.tradeAmount[id] !== sellOfTrade && tokenDecimals != 0) {
        const oneNanoLessPRVToGet = await priceCalculator.priceToBuyXPRV({
          tokenSymbol: a.tokenSymbol[id],
          PRVToBuy: (BigInt(tradeResult.moveDecimalDot(9)) + 1n).moveDecimalDot(-9),
          truncateToMaxDecimals: false,
          roundOne: true,
        });

        if (tradeResult.moveDecimalDot(9) - 1 !== 0)
          message =
            `\n__*Sell*__ \`${numberWithCommas(oneNanoLessPRVToGet)}\` ${a.tokenSymbol[id]} ` +
            `__*to get*__ \`${numberWithCommas(
              leading0s((BigInt(tradeResult.moveDecimalDot(9)) + 1n).moveDecimalDot(-9))
            )}\` PRV.`;
      }
      a.tradeAmount[id] = sellOfTrade;
    } else {
      // If you try to buy more than what is actually in liquidity, set your tradeAmount to a realistic value
      if (BigInt(a.tradeAmount[id].moveDecimalDot(tokenDecimals).toFixedS(0)) >= intToken)
        a.tradeAmount[id] = (intToken - 1).moveDecimalDot(-tokenDecimals);

      tradeResult = await priceCalculator.priceToBuyXTokens({
        tokenSymbol: a.tokenSymbol[id],
        tokensToBuy: a.tradeAmount[id],
      });

      const tokensGivenByThosePRV = await priceCalculator.priceOfSellXPRV({
        tokenSymbol: a.tokenSymbol[id],
        truncateToMaxDecimals: false,
        PRVToSell: tradeResult,
        roundOne: true,
      });

      if (
        BigInt(tokensGivenByThosePRV.moveDecimalDot(tokenDecimals)) >
        BigInt(a.tradeAmount[id].moveDecimalDot(tokenDecimals))
      )
        a.tradeAmount[id] = tokensGivenByThosePRV;
    }
  } else {
    if (a.action[id] === "Sell") {
      const minPRVToBuy = priceCalculator.getMinPrvToSell();
      if (BigInt(a.tradeAmount[id].moveDecimalDot(9)) < BigInt(minPRVToBuy.moveDecimalDot(9))) {
        message += `\n_That is the min PRV you can sell, due to the fees._`;
        a.tradeAmount[id] = minPRVToBuy;
      }
      // How many tokens you get by selling PRV
      tradeResult = await priceCalculator.priceOfSellXPRV({
        tokenSymbol: a.tokenSymbol[id],
        truncateToMaxDecimals: false,
        PRVToSell: a.tradeAmount[id],
        roundOne: true,
      });

      // How many PRV you need to sell to buy those tokens
      const prvToBuyThoseTokens = await priceCalculator.priceToBuyXTokens({
        tokenSymbol: a.tokenSymbol[id],
        tokensToBuy: !(tradeResult === "0") ? tradeResult : minToken,
      });

      // If there is a better trade for the same amount
      if (
        BigInt(a.tradeAmount[id].moveDecimalDot(9)) - BigInt(prvToBuyThoseTokens.moveDecimalDot(9)) != 0 &&
        tradeResult != "0"
      ) {
        message =
          `\n\nIf you sell \`${numberWithCommas(leading0s(a.tradeAmount[id]))}\` PRV, ` +
          `you'll still get \`${numberWithCommas(tradeResult)}\` ` +
          `${a.tokenSymbol[id]},\nbut you'll be loosing ` +
          `\`${numberWithCommas(
            leading0s(
              (
                BigInt(a.tradeAmount[id].moveDecimalDot(9)) - BigInt(prvToBuyThoseTokens.moveDecimalDot(9))
              ).moveDecimalDot(-9)
            )
          )}\` PRV.`;
        a.tradeAmount[id] = prvToBuyThoseTokens;
      } else if (tradeResult === "0") {
        message = `\n\nAnd that is the minimum you need to buy \`${minToken}\` ${a.tokenSymbol[id]}.`;
        tradeResult = minToken;
        a.tradeAmount[id] = prvToBuyThoseTokens;
      }
    } else {
      // If you try to buy more than what is actually in liquidity, set your tradeAmount to a realistic value
      if (BigInt(a.tradeAmount[id].moveDecimalDot(9)) >= intPRV)
        a.tradeAmount[id] = (intPRV - 1).moveDecimalDot(-9);

      tradeResult = await priceCalculator.priceToBuyXPRV({
        tokenSymbol: a.tokenSymbol[id],
        truncateToMaxDecimals: false,
        PRVToBuy: a.tradeAmount[id],
      });
      let sellOfTrade = await priceCalculator.priceOfSellXTokens({
        tokenSymbol: a.tokenSymbol[id],
        tokensToSell: tradeResult,
      });
      if (BigInt(sellOfTrade.moveDecimalDot(9)) >= intPRV) sellOfTrade = (intPRV - 1).moveDecimalDot(-9);

      if (a.tradeAmount[id] < Number(sellOfTrade) && Number(tradeResult) >= 2) {
        const betterTradeResult = await priceCalculator.priceOfSellXTokens({
          tokenSymbol: a.tokenSymbol[id],
          tokensToSell: (BigInt(tradeResult.moveDecimalDot(tokenDecimals)) - 1n).moveDecimalDot(-tokenDecimals),
        });
        message =
          `\n*Buy* \`${numberWithCommas(betterTradeResult)}\` PRV selling ` +
          `\`${numberWithCommas(
            (BigInt(tradeResult.moveDecimalDot(tokenDecimals)) - 1n).moveDecimalDot(-tokenDecimals)
          )}\` ${a.tokenSymbol[id]}.`;
      }
      a.tradeAmount[id] = sellOfTrade;
    }
  }

  const tradeAmountToShow = numberWithCommas(leading0s(a.tradeAmount[id]));
  const tradeResultToShow = numberWithCommas(tradeResult);
  message =
    `*${a.action[id] === "Sell" ? "__Sell__" : "__Buy__"}* \`${tradeAmountToShow}\` ` +
    `${a.reverse[id] ? a.tokenSymbol[id] : "PRV"} ` +
    `${a.action[id] === "Sell" ? "__to get__" : "__selling__"} \`${tradeResultToShow}\` ` +
    `${a.reverse[id] ? "PRV" : a.tokenSymbol[id]}.${message}`;

  const prvsInPoolAfterTrade = (a.reverse[id]
    ? BigInt(intPRV) - BigInt(tradeResult.moveDecimalDot(9)) * BigInt(a.action[id] === "Sell" ? 1 : -1)
    : BigInt(intPRV) - BigInt(a.tradeAmount[id].moveDecimalDot(9)) * BigInt(a.action[id] === "Sell" ? -1 : 1)
  ).moveDecimalDot(-9);

  const tokensInPoolAfterTrade = (a.reverse[id]
    ? BigInt(intToken) -
      BigInt(a.tradeAmount[id].moveDecimalDot(tokenDecimals)) * BigInt(a.action[id] === "Sell" ? -1 : 1)
    : BigInt(intToken) -
      BigInt(tradeResult.moveDecimalDot(tokenDecimals)) * BigInt(a.action[id] === "Sell" ? 1 : -1)
  ).moveDecimalDot(-tokenDecimals);

  const actualReverse = (BigInt(intPRV.moveDecimalDot(tokenDecimals)) / BigInt(intToken)).moveDecimalDot(-9);
  const actualNotReverse = (
    BigInt(intToken.moveDecimalDot(9 + (9 - tokenDecimals))) / BigInt(intPRV)
  ).moveDecimalDot(-9);

  const futureReverse = (
    BigInt(prvsInPoolAfterTrade.moveDecimalDot(18)) / BigInt(tokensInPoolAfterTrade.moveDecimalDot(9))
  ).moveDecimalDot(-9);
  const futureNotReverse = (
    BigInt(tokensInPoolAfterTrade.moveDecimalDot(18)) / BigInt(prvsInPoolAfterTrade.moveDecimalDot(9))
  ).moveDecimalDot(-9);

  const changeReverse = (
    BigInt(actualReverse.moveDecimalDot(9).toFixedS(0)) - BigInt(futureReverse.moveDecimalDot(9).toFixedS(0))
  )
    .abs()
    .moveDecimalDot(-9)
    .toFixedS(9);
  const changeNotReverse = (
    BigInt(actualNotReverse.moveDecimalDot(9)) - BigInt(futureNotReverse.moveDecimalDot(9))
  )
    .abs()
    .moveDecimalDot(-9)
    .toFixedS(9);

  const percentageReverse = ((changeReverse * 100) / actualReverse).toFixedS(9);
  let percentageReverseToShow = percentageReverse.toFixedS(2);
  for (let i = 3; Number(percentageReverseToShow) === 0 && i <= 9; i++)
    percentageReverseToShow = percentageReverse.toFixedS(i);
  percentageReverseToShow = numberWithCommas(leading0s(percentageReverseToShow));

  const percentageNotReverse = ((changeNotReverse * 100) / actualNotReverse).toFixedS(9);
  let percentageNotReverseToShow = percentageNotReverse.toFixedS(2);
  for (let i = 3; Number(percentageNotReverseToShow) === 0 && i <= 9; i++)
    percentageNotReverseToShow = percentageNotReverse.toFixedS(i);
  percentageNotReverseToShow = numberWithCommas(leading0s(percentageNotReverseToShow));

  message =
    `${message}\n\n` +
    `*Price after trade*:\n` +
    `· ${a.tokenSymbol[id]}: ` +
    `\`${numberWithCommas(leading0s(futureReverse.toFixedS(9)))}\` ` +
    `PRV \\(\`${Number(actualReverse) < futureReverse ? "\\+" : "-"}${percentageReverseToShow}\`%\\).\n` +
    `· PRV: \`${numberWithCommas(leading0s(futureNotReverse.toFixedS(9)))}\` ` +
    `${a.tokenSymbol[id]} ` +
    `\\(\`${Number(actualNotReverse) < futureNotReverse ? "\\+" : "-"}${percentageNotReverseToShow}\`%\\).\n\n`;

  message +=
    `*Pool after trade*:\n` +
    `· PRV: \`${numberWithCommas(leading0s(prvsInPoolAfterTrade.toFixedS(9)))}\`.\n` +
    `· ${a.tokenSymbol[id]}: \`${numberWithCommas(leading0s(tokensInPoolAfterTrade.toFixedS(9)))}\`.`;

  return (message + "\n\n_Send any other amount to calculate._").replace(/\./g, "\\.");
}

function setVariables(data, id) {
  let [, tokenSymbol, backHome, reverse, tradeAmount, action] = data.split("_");
  a.tokenSymbol[id] = tokenSymbol;
  a.backHome[id] = backHome;
  a.reverse[id] = reverse === "true";
  a.tradeAmount[id] = tradeAmount;
  a.action[id] = action;
  a.usingCalculator[id] = true;
  if (
    BigInt(a.tradeAmount[id].moveDecimalDot(9).toFixedS(0)) > BigInt("9007199254740991000000000".moveDecimalDot(9))
  )
    a.tradeAmount[id] = "9007199254740991";
  if (BigInt(a.tradeAmount[id].moveDecimalDot(9).toFixedS(0)) < 1) a.tradeAmount[id] = "0.000000001";
}

a.actionCalculator = async function (ctx, next, editMessage = true, isSaving = false, openingCalculator) {
  try {
    const id = ctx.from.id;
    let options = {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: "\u2716 Close",
              callback_data: "exitCalculator",
            },
          ],
        ],
      },
      parse_mode: "Markdown",
      disable_web_page_preview: true,
    };
    let deleteLastMessages = false;

    const updateMessageText = ctx.update?.message?.text?.replace(/,/g, "");
    if (editMessage || isSaving || openingCalculator) {
      setVariables(ctx.update.callback_query.data, id);
    } else if (a.usingCalculator[id] && /^(\d+\.?\d*|\d*\.?\d+)$/.test(updateMessageText)) {
      // The regex there ensures the message is a valid number
      a.tradeAmount[id] = updateMessageText.toFixedS(9);

      if (
        BigInt(a.tradeAmount[id].moveDecimalDot(9).toFixedS(0)) >
        BigInt("9007199254740991000000000".moveDecimalDot(9))
      ) {
        await reply(
          ctx,
          `Sorry, but that amount is too big for this calculator, or even for Incognito itself.\n\n` +
            "_Send any other amount to calculate._",
          options
        );
        return;
      }
      if (BigInt(a.tradeAmount[id].moveDecimalDot(9).toFixedS(0)) < 1) {
        await reply(
          ctx,
          `Sorry, but that amount is too small. It needs to be at least a nano: \`0.000000001\`.\n\n` +
            "_Send any other amount to calculate._",
          options
        );
        return;
      }

      deleteLastMessages = true;
    } else if (a.usingCalculator[id] && a.tokenSymbol[id]) {
      await reply(ctx, "Only positive numbers, please.", options);
      return;
    } else {
      return next(ctx);
    }

    const message = await getMessageText(id);

    const buttons = [
      [
        {
          text: `\u{1F501} Reverse`,
          callback_data:
            `c_${a.tokenSymbol[id]}_${a.backHome[id]}_` + `${!a.reverse[id]}_${a.tradeAmount[id]}_${a.action[id]}`,
        },
        {
          text: a.action[id] === "Sell" ? "Buy" : "Sell",
          callback_data:
            `c_${a.tokenSymbol[id]}_${a.backHome[id]}_` +
            `${a.reverse[id]}_${a.tradeAmount[id]}_${a.action[id] === "Sell" ? "Buy" : "Sell"}`,
        },
      ],
      [
        {
          text: "\u2716 Close",
          callback_data: "exitCalculator",
        },
        {
          text: `\u{1F4BE} Save`,
          callback_data: `cs_${a.tokenSymbol[id]}_${a.backHome[id]}_${a.reverse[id]}_${a.tradeAmount[id]}_${a.action[id]}`,
        },
      ],
    ];

    options = {
      reply_markup: {
        inline_keyboard: buttons,
      },
      parse_mode: "MarkdownV2",
      disable_web_page_preview: true,
    };

    if (editMessage || openingCalculator) ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

    if (editMessage) {
      await ctx.editMessageText(message, options);
    } else await reply(ctx, message, options);

    // This IIFE async function deletes the last two messages at the same time.
    if (deleteLastMessages)
      await (async function () {
        return new Promise((resolve, reject) => {
          let messagesDeleted = 0;
          for (let i = 0; i <= 1; i++)
            bot.telegram
              .deleteMessage(ctx.from.id, ctx.message.message_id - i)
              .finally(() => ++messagesDeleted === 2 && resolve())
              .catch(
                (err) => err.response.description === "Bad Request: message to delete not found" && reject(err)
              );
        });
      })();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionCalculatorSave = async function (ctx, next) {
  setVariables(ctx.update.callback_query.data, ctx.from.id);
  const message = await getMessageText(ctx.from.id).catch((err) => handleErr(err, ctx));
  await ctx.editMessageText(message, { parse_mode: "MarkdownV2" }).catch((err) => handleErr(err, ctx));
  a.actionCalculator(ctx, next, false, true);
};

a.actionCalculatorExit = function (ctx) {
  a.usingCalculator[ctx.from.id] = false;
  ctx.deleteMessage().catch((err) => handleErr(err, ctx));
};

module.exports = a;
