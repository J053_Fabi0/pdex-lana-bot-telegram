/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const axios = require("axios");
const { tokensDB } = require("../../main");

const PRV_ID = "0000000000000000000000000000000000000000000000000000000000000004";

const a = {};

a.getpCustomTokens = async function () {
  const { data } = await axios.get("https://api.incognito.org/pcustomtoken/list");
  if (data.Error) throw data.Error;
  return data.Result;
};

a.getpTokens = async function () {
  const { data } = await axios.get("https://api.incognito.org/ptoken/list");
  if (data.Error) throw data.Error;
  return data.Result;
};

/**
 * Returns an object containing all information of the tokens, with the token id as key and all info as value.
 */
a.getAllTokens = async function () {
  const merge = {};
  const list = await this.getpTokens();
  const customList = await this.getpCustomTokens();

  for (const key of Object.keys(list)) merge[list[key].TokenID] = list[key];
  for (const key of Object.keys(customList)) {
    if (customList[key].TokenID in merge) continue;
    merge[customList[key].TokenID] = customList[key];
  }

  return merge;
};

/**
 * Checks if the pair exists in the pool. Omitting the second parameter assumes you are asking against PRV. The order of the tokens id does not matter.
 */
a.doesPairExists = async function (tokenID) {
  const PRV = "0000000000000000000000000000000000000000000000000000000000000004";
  const pDEXStatus = require("../pDEXStatus/pDEXStatus");
  const { PDEPoolPairs: pools } = await pDEXStatus.getFullpDEXStatus();
  const [, height] = Object.keys(pools)[0].split("-");
  return (
    Boolean(pools?.[`pdepool-${height}-${tokenID}-${PRV}`]?.Token1PoolValue) ||
    Boolean(pools?.[`pdepool-${height}-${PRV}-${tokenID}`]?.Token1PoolValue)
  );
};

a.getTokenDecimalsFromSymbol = async function (symbol) {
  let tokenDecimals;

  if (symbol === "PRV") tokenDecimals = 9;
  else {
    const { decimals } = await tokensDB.findOne({ symbol: symbol });
    tokenDecimals = decimals;
  }

  return tokenDecimals;
};

a.getTokenIDFromSymbol = async function (symbol) {
  let tokenID;

  if (symbol === "PRV") tokenID = PRV_ID;
  else {
    const { _id } = await tokensDB.findOne({ symbol: symbol });
    tokenID = _id;
  }

  return tokenID;
};

module.exports = a;
