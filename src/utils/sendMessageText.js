/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const { bot } = require("../../main");
const util = require("util");

const SECOND = 1000;
const minTimeBetweenSendChunk = SECOND * 3;
const sleep = util.promisify(setTimeout);
const maxMessagesToSendPerUser = 10;
const maxMessagesToSend = 29;
const messagesToSend = [];
const resolveAfter = 3000;
let mainCalled = false;

module.exports = async (ctxOrChatId, message, extra) =>
  new Promise((resolve, reject) => {
    messagesToSend.push([ctxOrChatId, message, extra, (res) => resolve(res), (err) => reject(err)]);
    if (!mainCalled && (mainCalled = true)) main();
  });

const main = async () => {
  if (messagesToSend.length > 0) {
    // If it has messages to send, send them
    await sendChunk();

    // And if after that, it still have messages, wait minTimeBetweenSendChunk and call main again
    if (messagesToSend.length > 0) {
      await sleep(minTimeBetweenSendChunk);
      main();
      // Always make mainCalled false if there are no messages
    } else mainCalled = false;
  } else mainCalled = false;
};

const sendChunk = () =>
  new Promise((resolve) => {
    setTimeout(() => resolve(), resolveAfter);

    const userMessages = {}; // Store how many messages are being send to a every user in this chunk
    const chunkToSend = []; // The messages will be sent
    const indexOfSendedMessages = []; // Store the indexes of the messages that will be sent, to delete them after that

    for (let i = 0; i < messagesToSend.length && chunkToSend.length < maxMessagesToSend; i++) {
      let [ctxOrChatId] = messagesToSend[i];
      ctxOrChatId = ctxOrChatId?.from?.id || ctxOrChatId; // Get the userID

      // If the user hasn't been schedule to receive more than the maxMessagesToSendPerUser, add the message to the chunkToSend
      if ((userMessages[ctxOrChatId] || 0) < maxMessagesToSendPerUser) {
        userMessages[ctxOrChatId] = userMessages[ctxOrChatId] ? userMessages[ctxOrChatId] + 1 : 1;
        chunkToSend.push(messagesToSend[i]);
        indexOfSendedMessages.unshift(i);
      }
    }

    // Delete the messages that are schedule to be sended and are already in chunkToSend
    for (const index of indexOfSendedMessages) messagesToSend.splice(index, 1);

    // This function will resolve the promise after the numberOfMessagesDelivered are equal to the chunkToSend.length
    let numberOfMessagesDelivered = 0;
    const messageDelivered = () => {
      if (++numberOfMessagesDelivered >= chunkToSend.length) return resolve();
    };

    // Deliver each message asynchronously, calling messageDelivered each time one message was delivered
    for (const message of chunkToSend) {
      if (Object.prototype.hasOwnProperty.call(message[0], "reply")) {
        usingCtx(...message).finally(() => messageDelivered());
      } else if (typeof ctxOrChatId == "number" || !isNaN(Number(message[0]))) {
        usingBot(...message).finally(() => messageDelivered());
      } else {
        message[4]("No ctx or user_id given.");
        messageDelivered();
      }
    }
  });

const usingCtx = async (ctx, message, extra, resolve, reject) => {
  try {
    const res = await ctx.reply(message, extra);
    resolve(res);
  } catch (err) {
    reject(err);
  }
};
const usingBot = async (chatId, message, extra, resolve, reject) => {
  try {
    const res = await bot.telegram.sendMessage(chatId, message, extra);
    resolve(res);
  } catch (err) {
    reject(err);
  }
};
