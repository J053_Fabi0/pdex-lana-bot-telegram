/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

module.exports = {
  startMessage:
    "Track the price of your favorites pairs in the pDEX market \u{1F60E}.\n\n" +
    "I'm free software under the GPLv3 license: https://gitlab.com/J053_Fabi0/pdex-lana-bot-telegram.",
  specialEmoji: "",
};
