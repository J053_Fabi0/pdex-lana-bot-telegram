/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram). If not, see <https://www.gnu.org/licenses/>.
*/

const menu = require("../utils/keyboards");
const handleErr = require("../utils/handleError");
const { specialEmoji: SE } = require("../utils/messages");
const mySubscriptions = require("../scenes/mySubscriptions");
const reply = require("../utils/sendMessageText");

module.exports = {
  setActions: function (bot) {
    bot.action(/^goPair_/, (ctx) => {
      const [, tokenSymbol, backHome] = ctx.update.callback_query.data.split("_");
      mySubscriptions.setGoPair({ tokenSymbol, backHome });
      mySubscriptions.enter(ctx);
    });
    bot.action("backHome", (ctx) => mySubscriptions.enter(ctx, true));
    bot.action(/^reverse_/, (ctx) => mySubscriptions.actionReverse(ctx));
    bot.action(/^decimals_/, (ctx) => mySubscriptions.actionDecimals(ctx));
    bot.action(/^onlyOnce_/, (ctx) => mySubscriptions.actionOnlyOnce(ctx));
    bot.action(/^select_/, (ctx) => mySubscriptions.actionSelectPair({ ctx }));
    bot.action(/^unsubscribe_/, (ctx) => mySubscriptions.actionUsubscribe(ctx));
    bot.action(/^hideOptions_/, (ctx) => mySubscriptions.actionHideOptions(ctx));
    bot.action(/^customAlert_/, (ctx) => mySubscriptions.actionCustomAlert(ctx));
    bot.action(/^selectAlert_/, (ctx) => mySubscriptions.actionSelectAlert(ctx));
    bot.action(/^deleteAlert_/, (ctx) => mySubscriptions.actionDeleteAlert(ctx));
    bot.action(/^alertActive_/, (ctx) => mySubscriptions.actionAlertActive(ctx));
    bot.action(/^backPairInfo_/, (ctx) => mySubscriptions.actionBackPairInfo(ctx));
    bot.action(/^notifications_/, (ctx) => mySubscriptions.actionNotifications(ctx));
    bot.action("exitCalculator", (ctx) => mySubscriptions.actionCalculatorExit(ctx));
    bot.action(/^newCustomAlert_/, (ctx) => mySubscriptions.actionNewCustomAlert(ctx));
    bot.action(/^myCustomAlerts_/, (ctx) => mySubscriptions.actionMyCustomAlerts(ctx));
    bot.action(/^changeDecimals_/, (ctx) => mySubscriptions.actionChangeDecimals(ctx));
    bot.action(/^cs_/, (ctx, next) => mySubscriptions.actionCalculatorSave(ctx, next));
    bot.action(/^c_/, (ctx, next) => mySubscriptions.actionCalculator(ctx, next, true));
    bot.action(/^everyTradeAlert_/, (ctx) => mySubscriptions.actionEveryTradeAlert(ctx));
    bot.action(/^customTradeAlert_/, (ctx) => mySubscriptions.actionCustomTradeAlert(ctx));
    bot.action(/^greaterOrLessThan_/, (ctx) => mySubscriptions.actionGreaterOrLessThan(ctx));
    bot.action(/^specificPriceAlert_/, (ctx) => mySubscriptions.actionSpecificPriceAlert(ctx));
    bot.action(/^ce_/, (ctx, next) => mySubscriptions.actionCalculator(ctx, next, false, false, true));

    // Leave any scene and open keyboard menu
    bot.action(["leave", "cancel"], async (ctx) => {
      try {
        ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
        ctx.deleteMessage().catch((err) => handleErr(err, ctx));
        await reply(ctx, `${SE || "\u{1F3E0}"}`, menu);
        await ctx.scene.leave();
      } catch (err) {
        handleErr(err, ctx);
      }
    });

    // Default actions
    bot.action(/.*/, async (ctx) => {
      try {
        ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
        await reply(
          ctx,
          "Seems that you've tried to interact with an old message. Please try again from the begining.",
          menu
        );
        await ctx.scene.leave();
      } catch (err) {
        handleErr(err, ctx);
      }
    });
  },
};
