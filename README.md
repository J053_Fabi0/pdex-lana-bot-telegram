# pDEX Lana Bot (Telegram)

Track the price of your favorites pairs in the pDEX market.

## Host it yourself


Download the code and create a file called .env with the following format:
```
TOKEN=
ADMIN_ID=
ADMIN_ALIAS=
FULLNODE_DIRECTION=
```

TOKEN is the token id that [@BotFather](https://t.me/botfather) gives you when you create a new bot.

You can get your ADMIN\_ID by sending a message to [@userinfobot](https://t.me/userinfobot).

The ADMIN\_ALIAS is your Telegram alias.

The FULLNODE\_DIRECTION is the link to your fullnode. [How to run Incognito Chain Full Node on Mainnet](https://we.incognito.org/t/how-to-run-incognito-chain-full-node-on-mainnet/1204).

Run `npm i`.

Then simply run `node main.js`.

---

## License
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
