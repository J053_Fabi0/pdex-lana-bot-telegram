/*
  This file is part of pDEX Lana Bot (Telegram) - Ensi Telegram Bot.

  pDEX Lana Bot (Telegram) is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pDEX Lana Bot (Telegram) is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pDEX Lana Bot (Telegram).  If not, see <https://www.gnu.org/licenses/>.
*/

const dotenv = require("dotenv");
dotenv.config();

const Datastore = require("nedb-promises");

const pricesDB = Datastore.create({ filename: "./db/prices.db", autoload: true });
pricesDB.persistence.setAutocompactionInterval(86_400_000); // Compacts database every day
exports.pricesDB = pricesDB;

const tokensDB = Datastore.create({ filename: "./db/tokens.db", autoload: true });
tokensDB.persistence.setAutocompactionInterval(86_400_000); // Compacts database every day
exports.tokensDB = tokensDB;

const peopleDB = Datastore.create({ filename: "./db/people.db", autoload: true });
peopleDB.persistence.setAutocompactionInterval(86_400_000); // Compacts database every day
exports.peopleDB = peopleDB;

const Telegraf = require("telegraf");
const bot = new Telegraf(process.env.TOKEN);
exports.bot = bot;

const menu = require("./src/utils/keyboards");
const Stage = require("telegraf/stage");
const session = require("telegraf/session");

const handleErr = require("./src/utils/handleError");

const pDEXStatus = require("./src/pDEXStatus/pDEXStatus");

bot.catch((err, ctx) => handleErr(err, ctx));

// Add all scenes to the stage
const scenes = require("./src/scenes/scenes");
const stage = new Stage([...Object.values(scenes)]);

bot.use(session()).catch((err) => console.log(err));
bot.use(stage.middleware()).catch((err) => console.log(err));

const commands = require("./src/commands/commands");
commands.setCommands(bot);

const actions = require("./src/actions/actions");
actions.setActions(bot);

const hears = require("./src/hears/hears");
hears.setHears(bot);

const reply = require("./src/utils/sendMessageText");
bot.on("message", (ctx) =>
  reply(ctx, "Please, use one of the options from the custom keyboard.", menu).catch((err) => handleErr(err))
);

bot
  .launch()
  .then(() => console.log("Bot launched.\n"), pDEXStatus.main(true))
  .catch((err) => console.log(err));
